import tweepy
import time
import json
import os

consumer_key = ""
consumer_key_secret = ""
access_token = ""
access_token_secret = ""

auth = tweepy.OAuthHandler(consumer_key, consumer_key_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=False)

user= "waynerooney"
f = open("profili/"+user+".txt", "a")  
try:  
    for tweet in tweepy.Cursor(api.user_timeline, screen_name=user, exclude_replies=True, count = 10).items():  
        tweet_text = tweet.text  
        time = tweet.created_at  
        tweeter = tweet.user.screen_name  
        tweet_dict = {"tweet_text" : tweet_text.strip()	}  
        tweet_json = json.dumps(tweet_dict)  
        f.write(tweet_text)
        f.flush()
        print(tweet_text)  
except tweepy.TweepError:  
    time.sleep(60)



